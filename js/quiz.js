const next = document.querySelector('.next');
const start = document.querySelector('.start');
const txt = document.querySelectorAll('.txt');
const question = document.getElementById("question");
const CorrectAnswerTxt = document.getElementById("correctanswer");
const result = document.querySelector(".result")
const form = document.querySelector(".form");
const Previus = document.querySelector(".Previus");
const finish = document.querySelector(".finish")
const examResult = document.querySelector(".examresult")
const myQuestions = [
    {
    question: "Who invented JavaScript?",
    answers: {
        a: "Douglas Crockford",
        b: "Sheryl Sandberg",
        c: "Brendan Eich"
    },
    correctAnswer: "c"
    },
    {
    question: "Which one of these is a JavaScript package manager?",
    answers: {
        a: "Node.js",
        b: "TypeScript",
        c: "npm"
    },
    correctAnswer: "c"
    },
    {
    question: "Which tool can you use to ensure code quality?",
    answers: {
        a: "Angular",
        b: "jQuery",
        c: "RequireJS",
        d: "ESLint"
    },
    correctAnswer: "d"
    }
];


let  number = 0
let trueAnswer = 0
let falseAnswer = 0
let answeredAll = 0

function myQuizQuetions(){
    myQuestions.forEach((currentQuestion, i) =>{
        i === number ? question.innerHTML = currentQuestion.question : console.log(false)
    })
    
    myQuestions.forEach((currentAnswers, i) =>{ 

        if( i == number){
            const text =`<div class="label">
            <input type="radio" class="inpt"  value="a"   >
            <p>A</p>
            <p class="txt">${currentAnswers.answers.a}</p>
        </div>
        <div class="label">
            <input type="radio" class="inpt"  value="b"   >
            <p>B</p>
            <p class="txt">${currentAnswers.answers.b}</p>
        </div>
        <div class="label">
            <input type="radio" class="inpt"  value="c"   >
            <p>C</p>
            <p class="txt">${currentAnswers.answers.c}</p>
        </div>
        <div class="label">
            <input type="radio" class="inpt"  value="d"  >
            <p>D</p>
            <p class="txt">${currentAnswers.answers.d}</p>
        </div>`
            form.innerHTML = text
        }
    })
}

function myQuiz(){
    const inpts = document.querySelectorAll('.inpt');

    for (let i = 0; i < inpts.length; i++) {
    inpts[i].addEventListener('click', function(e){
        
        answeredAll++ 

        answeredAll == myQuestions.length  ? finish.style.opacity = "1" : console.log(false)
        answeredAll == myQuestions.length  ? Previus.style.opacity  = "1" : console.log(false)
        myQuestions.forEach((correctAnswers,index) =>{

            if(index === number){
                correctAnswers.correctAnswer === inpts[i].value ? trueAnswer++ : falseAnswer++
                correctAnswers.correctAnswer === inpts[i].value ? result.innerHTML = "true" : result.innerHTML = "false"
                correctAnswers.correctAnswer !== inpts[i].value ? CorrectAnswerTxt.innerHTML = correctAnswers.correctAnswer : CorrectAnswerTxt.innerHTML = "" 
            }

        })
    })
}
    result.innerHTML = ""
    CorrectAnswerTxt.innerHTML = ''
}
myQuizQuetions()
myQuiz()

next.addEventListener('click',function(){

    number++
    myQuizQuetions()
    myQuiz()
    number == myQuestions.length - 1 ? next.style.opacity = "0" : console.log(false);
    console.log(number)
    console.log(myQuestions)

})
Previus.addEventListener('click',function(){
    number = 0
    myQuizQuetions()
    myQuiz()
    number == 0 ? Previus.style.opacity = "0" : console.log(false)
    number < myQuestions.length ? next.style.opacity = "1" : console.log(false)
    finish.style.opacity = "0"
    examResult.innerHTML = ""
    trueAnswer = 0
    falseAnswer = 0
    answeredAll = 0

})
finish.addEventListener("click", function(){

    trueAnswer > falseAnswer ? examResult.innerHTML = 'pass exam ' : examResult.innerHTML = "didn't pass exam"
    console.log(number)

})







